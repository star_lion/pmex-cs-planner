PMEX Champion Stadium Planner
---


- [ ] 0.2.0
    - [ ] data modeling
        - [ ] possible
            - [ ] round parameters
            - [ ] parameters
        - [ ] examples
            - [ ] complete week data
            - [ ] sample sync pairs


- [ ] 0.1.0
    - [x] project setup
        - [x] js linting
        - [x] postcss
            - [x] nesting
            - [x] variables
            - [x] tailwinds
        - [x] parcel
            - [x] fast refresh (on by default)
        - [x] data store solution (zustand or hookstate)
            - [x] serialization
    - [ ] basic layout w/ breakpoints, boilerplate styles
        - [x] mobile first design
            - [x] queries
            - [x] typography
            - [x] viewport configuration
        - [ ] tablets and larger


- [ ] unplanned certain
    - [ ] basic mode/master mode
    - [ ] some sort of roster management
    - [ ] integration with external tools (pokemas sync toool, sync grid helper, gamepress/wiki references)
    - [ ] picture exports
    - [ ] ai inferred data from screenshots
    - [ ] sharing
        - [ ] tags (type, stall, damage, healing, strategy, custom)
    - [ ] settings:
        - [ ] set position by list order (default)
        - [ ] set target by list order
    - [ ] rewards calculation
    - [ ] test building, maybe for static gh page


- [ ] unplanned potential
    - [ ] archive for past weeks
    - [ ] click points to toggle Total Points/Round Points
