import React from 'react'

import SyncPair from './SyncPair'

export default function Body () {
  return (
    <div className='body'>

      <div className='mobile-header'>
        <div className='leader-info bg-green-200'>
          <div>$name $weakness $attacks</div>
          <div>$league week $# Points: $#</div>
        </div>
        <div className='parameters bg-yellow-400'>Round $#: $info</div>
      </div>

      <div className='main'>

        <div className='league-info'>
          League Week Total Points
        </div>

        <div className='info-panel'>
          Info panel
        </div>

        <div className='leader'>
          <div>1 Leader</div>
          <div>Weakness</div>
          <div>Attack types</div>
        </div>
        <div className='team'>
          <SyncPair />
          <SyncPair />
          <SyncPair />
        </div>
      </div>

      <div className='info-aside'>
        Info aside
      </div>

      <div className='mobile-footer'>
        <div className='parameters bg-yellow-400'>Params: $params</div>
        <div className='control-bar bg-gray-300'>
          <button>$# $leader</button>
          <button>(pair)</button>
          <button>up</button>
          <button>menu</button>
        </div>
      </div>
    </div>
  )
}
