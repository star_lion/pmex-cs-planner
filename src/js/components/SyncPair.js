import React from 'react'

export default function SyncPair () {
  return (
    <div className='sync-pair'>
      <div>
        Trainer
        <br />
        &
        <br />
        Pokemon
      </div>
      <div>
        <div>role, weakness</div>
        <div>type(s)</div>
      </div>
      <div>
        sync grid
      </div>
      <div>
        <div>position</div>
        <div>target</div>
      </div>
    </div>
  )
}
