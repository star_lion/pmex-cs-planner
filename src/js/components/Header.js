import React from 'react'

export default function Header () {
  return (
    <div className='header bg-gray-300'>
      <button className='float-right'>settings</button>
      <h2> PMEX CS Planner </h2>
    </div>
  )
}
