import React from 'react'
import { useStoreByPaths } from '../store'

export default function Info () {
  const [infoVisibility, toggleInfo] = useStoreByPaths([
    ['ui', 'info', 'visible'],
    ['toggleInfo']
  ])

  return (
    <div className='border-solid border-2'>
      info view {`${infoVisibility}`}
      <div><button onClick={toggleInfo}>toggleInfo</button></div>
    </div>
  )
}
