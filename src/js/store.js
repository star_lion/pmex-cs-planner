import create from 'zustand'
import { persist } from 'zustand/middleware'
import produce from 'immer'
import { compose, paths } from 'ramda'

const initialState = {
  ui: {
    info: {
      visible: true
    }
  }
}

// start middleware
const log = (config) => (set, get, api) => config((args) => {
  // todo: toggleable log based on debug setting or config
  console.log('applying', args)
  set(args)
  console.log('new state', get())
}, get, api)

const immer = (config) => (set, get, api) => config((fn) => set(produce(fn)), get, api)

const serialize = (config) => persist(config, { name: 'pmex-cs-planner-data' })
// end middleware

const createStore = compose(
  create,
  serialize,
  immer,
  log
)

const useStore = createStore(set => ({
  ...initialState,
  toggleInfo: () => set((state) => { state.ui.info.visible = !state.ui.info.visible })
}))

export const useStoreByPaths = (storePaths) => useStore(paths(storePaths))

export default useStore
